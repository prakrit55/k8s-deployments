terraform {
    required_version = "~> 1.3"

    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.0"
        }
    }
}

module "ecr1" {
    source = "./module/ecr"
    ecr_repo_name = local.ecr_repo_name_1
}

module "ecr2" {
    source = "./module/ecr"
    ecr_repo_name = local.ecr_repo_name_2
}


module "eks" {
    source = "./module/eks"
    cluster_name = local.cluster
    iam_demo_role = local.iam_demo_role
}

# module "ingress" {
#     source = "./module/ingress"
# }