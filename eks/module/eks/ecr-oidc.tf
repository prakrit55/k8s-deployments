data "aws_iam_policy_document" "ecr_reg_assume_role_policy" {
    statement {
        actions = ["sts:AssumeRoleWithWebIdentity"]
        effect  = "Allow"

    condition {
        test     = "StringEquals"
        variable = "${replace(aws_iam_openid_connect_provider.eks.url, "https://", "")}:sub"
        values   = [
            "system:serviceaccount:frontend:frontend",
            "system:serviceaccount:backend:backend"
        ]
    }

    principals {
        identifiers = [aws_iam_openid_connect_provider.eks.arn]
        type        = "Federated"
    }
    }
}

resource "aws_iam_role" "ecr_registry" {
    assume_role_policy = data.aws_iam_policy_document.ecr_reg_assume_role_policy.json
    name               = "ecr_registry"
}

resource "aws_iam_policy" "ecr_access_policy" {
    name = "ecr-access"

    policy = jsonencode({
    Statement = [
        {
        Effect = "Allow"
        Action = [
            "ecr:GetAuthorizationToken",
            "ecr:BatchGetImage",
            "ecr:DescribeImages",
            "ecr:ListImages",
        ]
        Resource = "*"
        },
        {
        Effect = "Allow"
        Action = [
            "ecr:BatchDeleteImage",
            "ecr:DeleteImage",
            "ecr:PutImage"
        ]
        Resource = "*"
        Condition = {
            "StringEquals" = {
            "aws:ResourceTag/k8s.io/cluster-autoscaler/${var.cluster_name}": "owned"
            }
        }
        }
    ]
    Version = "2012-10-17",
    })
}

resource "aws_iam_role_policy_attachment" "ecr_policy_attachment" {
    role       = aws_iam_role.ecr_registry.name
    policy_arn = aws_iam_policy.ecr_access_policy.arn
}

output "ecr_registry_arn" {
    value = aws_iam_role.ecr_registry.arn
}