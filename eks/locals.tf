locals {
    cluster = "demo"
    iam_demo_role = "eks-cluster-demo"
    ecr_repo_name_1 = "backend-app-ecr-repo"
    ecr_repo_name_2 = "frontend-app-ecr-repo"
}