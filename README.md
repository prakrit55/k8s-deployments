# k8s-deployments



In the repo, we store the manifests files for the frontend, backend and the database. Its basically the gitops repo where the frontend and backend repo push changes made to the application. ([frontend](https://gitlab.com/prakrit55/BlogApp-frontend), [backend](https://gitlab.com/prakrit55/blogapp-backend))

It contains directories such as `kustomize-frontend` and `kustomize-backend`, while pushing the changes such as changing image tags, kustomize replaces the image tags in the main directories frontend and backend respectively through the kustomize direcories.


![alt text](docs/tfppp.png)

The repository also contains `eks` terraform files to create the eks cluster. It also creates the `ECR` private registry to store the images. It contains the `oidc` permissions to be used while creating cluster autoscalers and ecr image access service accounts. The service accounts are associated with the pods to pull the images from ecr repo and the cluster autoscaler deployment can access the auto scaling groups to rollout and rollin nodes.

The ingress directory contains ingress manifests in order to access the application outside the world. So, it must be deployed first to create the nginx ingress. The ingress creates an aws loadbalancer to balance the traffic to the application in the eks.
For each application(frontend and backend) we must ensure ingress rules to connect to the application.

For the persistent volumes, we must ensure `csi driver` to be installed, to map the persistent volumes to use the ebs storage from the aws. To do that, docs in the `aws-csi-driver` could be used for installation.

For the cluster auto scaler, the deployment alongwith the serviceaccount must be resolved in the eks cluster. And the arn of the role for autoscaling should be added in the service account annotations.It helps to increase the number of nodes when the nodes are almost having low resource such as memory and cpu. Checkout the `k8s-cluster-autoscaler` directory for more.


![alt text](docs/argg.png)


The argocd must be installed with the help of the docs present in the argocd directory. The ingress rule must be mapped to the service(argocd-server) in the argocd namespace to expose the argocd dashboards.
The `argocd` in the cluster addresses the yamls in the argo directory to deploy in eks cluster. The argo directory contains manifests in subdirectory for the different parts of the microservices to deploy.




# Application Description




![alt text](docs/kk88s.png)


1. The application consists of a frontend, backend, and a database on mongodb.
2. When any user access through the loadbalancer endpoint to the frontend path in the ingress, the     frontend shows the default blog page, where the blogs written by users can be seen.
3. The frontend pod access the image from `ECR` registry by the help of `OIDC` permissions through the service account embedded in the deployment.
4. It sends a requests to the backend, through the `env` present in the deployment. 
5. The backend pod access the image through the same `Open Id Connect Provider` service accounts in the deployment.
6. The backend communicate through the `headless` service mapped to the statefulsets for mongodb databases. The headless service creates `dns` to communicate with the pods from statefulsets.
7. The backend pod uses a secret to login the database, for this configuration another secret in databse is being used to set a regular password.
6. The statefulset objects in kubernetes created pod in a order such as `mongodb-0, mongodb-1`, to preserve states and data of the application. The data gets replicated from the initial pod `mongodb-0` to `mongodb-1` while creation. The volume claim template inn the statefulset creates the volume based on the `ebs` storage type and memory specified in the persistent volume for database.
7. The configmap configures the credentials in the mongodb and the secret provides the password to be used by the backend while login the database.

# frontend
![alt text](docs/Screenshot(295).png)
![alt text](docs/Screenshot(302).png)

# backend
![alt text](docs/Screenshot(304).png)
![alt text](docs/Screenshot(305).png)

# database
![alt text](docs/Screenshot(306).png)
