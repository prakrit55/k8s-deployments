## ArgoCD Installation


kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}"

kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

argocd admin initial-password -n argocd

argocd login <ARGOCD_SERVER>
argocd account update-password

kubectl config get-contexts -o name
argocd cluster add docker-desktopkubectl delete


## ArgoRollouts Installations

install.yaml - Standard installation method.

kubectl create namespace argo-rollouts
kubectl apply -n argo-rollouts -f https://github.com/argoproj/argo-rollouts/releases/latest/download/install.yaml


Kubectl Plugin Installation
The kubectl plugin is optional, but is convenient for managing and visualizing rollouts from the command line.

** Install Argo Rollouts Kubectl plugin with curl.
curl -LO https://github.com/argoproj/argo-rollouts/releases/latest/download/kubectl-argo-rollouts-linux-amd64

** Make the kubectl-argo-rollouts binary executable.

chmod +x ./kubectl-argo-rollouts-linux-amd64

** Move the binary into your PATH.
sudo mv ./kubectl-argo-rollouts-linux-amd64 /usr/local/bin/kubectl-argo-rollouts

** Test to ensure the version you installed is up-to-date:
kubectl argo rollouts version